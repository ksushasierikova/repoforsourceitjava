package Lesson5;

/**
 * Created by P-C on 21.01.2018.
 */
public class Instructor {
    String name;
    String color;
    int age;

    public Instructor(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public boolean dance() {
        return true;
    }

    public int getAge(){
        return age;
    }

    public void setAge(int age) {
        this.age = age;
        age = 88;
    }

}
