package Lesson5;

import com.sourceit.arraysTest.ArrList;

/**
 * Created by P-C on 16.01.2018.
 */
public class ArrayWithoutLength {

    public static void main(String[] args) {

        ArrList arrList = new ArrList(4);
        arrList.add(1);
        arrList.add(2);
        arrList.add(3);
        arrList.add(4);
        arrList.add(5);
        arrList.remove(3);

        System.out.println("size after removal " + arrList.size());
//        System.out.println(arrList.get(0));
//        System.out.println(arrList.get(1));
//        System.out.println(arrList.get(2));
    }

}
