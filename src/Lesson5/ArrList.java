package Lesson5;

public class ArrList<T> {
    private T[] arr;
    int idx = 0;

    public ArrList(int size) {
        arr = (T[]) new Object[size];
    }

    public ArrList() {
        arr = (T[]) new Object[4];
    }

    public void add(T value) {
        if (idx >= arr.length) {
            increase();
        }
        arr[idx++] = (T) value;
    }

    // Hw: из ArrList взять недостающие методы и добавить в реализацию своей коллекции
    // реализовать методы из collaction
    // имплементировать пекеджнеймы
    public void increase() {
        Object temp[] = new Object[arr.length * 2];// увеличение можно вынести в отдельный метод, визуально будет легче понимать что происходит в коде.
        for (int i = 0; i < arr.length; i++) {
            temp[i] = arr[i];
        }
        arr = (T[]) temp;
    }

    public int size() {
        return idx;
    }

}
