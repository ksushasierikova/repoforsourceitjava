package Lesson5;

/**
 * Created by P-C on 21.01.2018.
 */
public class Inst_main {

    public static void main(String[] args) {

        Instructor instrF = new Instructor("Ivan", 45);
        System.out.println(instrF.getAge()); //тут getAge() выдаст 45

        int ageF = 56;
        // примитивы передаются по значению, т.е копируется его значение
        // тут java копирует значение переменной ageF и присваивает его в ageF. Это передача параметра по значению
        int ageS = ageF;
        instrF.setAge(ageS);
        System.out.println(instrF.getAge()); //тут getAge() выдаст 56

        // ссылки на объект передаются по значению, копируется значение самой ссылки на объект
        // ссылка instrF копируется в instrS. Объект Instructor один, а ссылки на него две
        Instructor instrS = instrF; //тут getAge() выдаст 56
        instrS.setAge(33);
        System.out.println(instrS.getAge()); // тут getAge() выдаст 33
    }

}
