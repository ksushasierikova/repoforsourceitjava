package Lesson5;

/**
 * Created by P-C on 16.01.2018.
 */
public class Person {
    public String firstName;
    public String lastName;
    public int age;
    public String proffession;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(String firstName, String lastName, int age, String proffession) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.proffession = proffession;
    }

    public Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public void Person(int age) {
        age++;
    }

    public Person(String proffession) {
        this.proffession = proffession;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", proffession='" + proffession + '\'' +
                '}';
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
