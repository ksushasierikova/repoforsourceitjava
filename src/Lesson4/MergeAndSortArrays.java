package Lesson4;

import java.util.Arrays;
import java.util.stream.IntStream;

public class MergeAndSortArrays {
    static int[] firstArr = {0, 1, 2, 5, 6};
    static int[] secArr = {3, 4, 7, 8, 9};

    public static void main(String[] args) {
        System.out.println(Arrays.toString(mergeArrays(firstArr, secArr)));
    }

    static int[] mergeArrays(int[] first, int[] second) {
        int[] res = new int[first.length + second.length];
        int indF = 0;
        int indS = 0;

        for (int i = 0; i < res.length; i++) {
            if (indF < first.length && indS < second.length) {
                res[i] = first[indF++];
            } else {
                res[i] = second[indS++];
            }
        }

        for (int i = 0; i < first.length; i++) {
            if (indF < first.length) {
                res[i] = first[indF++];
            }
        }

        for (int i = 0; i < second.length; i++) {
            if (indS < second.length) {
                res[i] = second[indS++];
            }
        }

        return res;
    }
}

//выбрать все значение больше 10
