package Lesson4;

import java.io.PrintWriter;
import java.util.Collection;
import java.util.Scanner;

/**
 * Created by P-C on 13.01.2018.
 */
public class Solution {
    public static void main(String[] args) {

        int[] a = {0, 1, 2, 5, 6};
        int[] b = {3, 4, 7, 8, 9};

        int[] result = new int[a.length + b.length];
        int i = 0, j = 0;
        int index = 0;
        while (i < a.length && j < b.length) {
            if (a[i] < b[j]) {
                result[index] = a[i];
                i++;
            } else {
                result[index] = b[j];
                j++;
            }
            index++;
        }
        while (i < a.length) {
            result[index] = a[i];
            index++;
            i++;
        }


        while (j < b.length) {
            result[index] = b[j];
            index++;
            j++;
        }
        for (int k = 0; k < a.length + b.length; k++) {
            System.out.println(result[k] + " ");
        }
    }
}
