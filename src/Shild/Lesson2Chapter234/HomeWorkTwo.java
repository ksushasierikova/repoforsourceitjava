package Shild.Lesson2Chapter234;

public class HomeWorkTwo {

    public static void main(String[] args) {

        float firstValue = 8.5f;
        float secondValue = 11.45f;
        int baseValue = 10;

       double result = findNearest(firstValue, secondValue, baseValue);
        System.out.println("nearest value is " + result);
       System.out.println(result == firstValue);
    }

    private static double findNearest(float first, float second, int base) {
        float c = Math.abs(base - first);
        float d = Math.abs(base - second);

        if (c > d) {
            return first;
        } else if (c == d) {
            return second;
        } else System.out.println("nearest to " + base + " is " + first);
        return 0;
    }
}
//shift+f6 - refactor