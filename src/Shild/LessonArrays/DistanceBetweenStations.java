package Shild.LessonArrays;

/**
 * Created by P-C on 10.01.2018.
 */
public class DistanceBetweenStations {
    // создать массив в котором уже будут вычислены значения межд станциями
    //check boundary values
    static int array[] = {5, 11, 8, 15, 7, 3, 5, 15, 9, 11};

    public static void main(String[] args) {
        System.out.println("Distance between the start and last stations is " + findPath(3, 9) + " miles");
    }

    public static int findPath(int startStation, int lastStation) {
        //int array[] = {5, 11, 8, 15, 7, 3, 5, 15, 9, 11};
        int sum = 0;

        for (int i = startStation; i <= lastStation ; i++) {
            sum = sum + array[i];
        }
        return sum;
    }

    //return cur = arr[laststat] - arr[firssta]

}

