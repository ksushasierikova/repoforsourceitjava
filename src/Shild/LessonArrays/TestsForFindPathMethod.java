package Shild.LessonArrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by P-C on 10.01.2018.
 */
public class TestsForFindPathMethod {

    @Test
    public void testFindPathMethod() {
        assertEquals(DistanceBetweenStations.findPath(2, 6), 38);
        assertEquals(DistanceBetweenStations.findPath(3, 9), 65);
    }

    @Test
    public void testFindPathMethod2() {
        assertNotSame(DistanceBetweenStations.findPath(2, 6), 33);
        assertNotSame(DistanceBetweenStations.findPath(3, 9), 33);
    }

}
