package com.sourceit.patternsStrategy;

import com.sourceit.iBehaviorPackage.IBehavior;

public class RedhadDuck extends AbsDuck {

    public RedhadDuck(IBehavior flyBehavior) {
        super(flyBehavior);
    }

    @Override
    void display(){
        System.out.println("");
    }

}
