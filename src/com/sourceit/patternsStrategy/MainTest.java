package com.sourceit.patternsStrategy;

import com.sourceit.iBehaviorPackage.NoFly;
import com.sourceit.iBehaviorPackage.WithFly;

public class MainTest {

        public static void main(String[] args) {
            AbsDuck donald = new DonaldDuck(new NoFly());
            AbsDuck redhad = new RedhadDuck(new WithFly());

            donald.display();
            donald.makeFly();

            redhad.display();
            redhad.makeFly();

        }

}
