package com.sourceit.patternsStrategy;

import com.sourceit.iBehaviorPackage.IBehavior;

abstract class AbsDuck{

    IBehavior flyBehavior;

    public AbsDuck(IBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void quack() {
        System.out.println("quack");
    }

    public void swim() {
        System.out.println("I'm swimming");
    }

    abstract void display();

    public void makeFly() {
        flyBehavior.fly();
    }

}
