package com.sourceit.patternsStrategy;

import com.sourceit.iBehaviorPackage.IBehavior;

public class DonaldDuck extends AbsDuck {

    public DonaldDuck(IBehavior flyBehavior) {
        super(flyBehavior);
    }

    @Override
    void display() {
        System.out.println("");
    }

}
