package com.sourceit.exceptionsExample;

public class Saturday extends MyException {
    final boolean isBad;

    public Saturday(boolean isBad) {
        this.isBad = isBad;
    }

    public void morning() throws MyException {
        if (isBad == true) {
            throw new MyException();
        }
        System.out.println("good morning");
    }

}
