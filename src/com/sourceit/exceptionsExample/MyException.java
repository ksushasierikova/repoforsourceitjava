package com.sourceit.exceptionsExample;

public class MyException extends Exception {
    String errorMessage = "bad message";

    @Override
    public String getMessage() {
        return errorMessage;
    }

}
