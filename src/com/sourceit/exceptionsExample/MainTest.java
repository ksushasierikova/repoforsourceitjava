package com.sourceit.exceptionsExample;

public class MainTest {

    public static void main(String[] args) throws MyException {
        Saturday mySat = new Saturday(false);

        //new Thread(() -> mySat.morning()).start();

        new Thread((new Runnable() {
            @Override
            public void run() {
                System.out.println();
            }
        })).start();

        try {
            mySat.morning();
            System.out.println("message from try");
        } catch (MyException e) {
            e.printStackTrace();
            System.out.println("message from catch");
        } finally {
            System.out.println("message from finally");
        }

    }
}