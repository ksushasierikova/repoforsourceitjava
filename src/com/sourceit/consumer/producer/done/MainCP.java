package com.sourceit.consumer.producer.done;

public class MainCP {

    public static void main(String[] args) {
        Store store = new Store();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    store.produce();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    store.consume();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
