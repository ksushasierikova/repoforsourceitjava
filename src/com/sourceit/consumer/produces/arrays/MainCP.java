package com.sourceit.consumer.produces.arrays;

/**
 * Created by P-C on 09.02.2018.
 */
public class MainCP {
    public static void main(String[] args) {

        Store store = new Store();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    store.produce();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                int result = 0;
                try {
                    store.consume();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(result);
            }
        }).start();
    }

}
