package com.sourceit.consumer.produces.arrays;

import java.util.LinkedList;

public class Store {

    int[] arrayStore = new int[10];
    //int[] res = new int[arrayStore.length];
    //LinkedList<Integer> arrayStore = new LinkedList<Integer>();
    Object lock = new Object();

    public void produce() throws InterruptedException {
        int goods = 0;
        int i = 0;
        while (true) {
            synchronized (lock) {
                if (arrayStore.length > 10) {
                    System.out.println("producer is sleeping");
                    lock.wait();
                    System.out.println("producer awake");
                }
                arrayStore[i] = arrayStore[goods];

                System.out.println("producer: " + goods);
                goods++;
                i++;
                lock.notifyAll();
            }
        }
    }

    public void consume() throws InterruptedException {
        int goods = arrayStore[0];
        while (true) {
            synchronized (lock) {
                if (arrayStore.length == 0) {
                    System.out.println("consumer is sleeping");
                    lock.wait();
                    System.out.println("consume awake");
                }
                int goodFromStore = arrayStore[goods--];
                System.out.println("consume: " + goodFromStore);
                lock.notifyAll();
            }
        }
    }
}

