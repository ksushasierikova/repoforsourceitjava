package com.sourceit.consumer.produser;

import java.util.LinkedList;

public class Store {

    LinkedList<Integer> store = new LinkedList<Integer>();
    Object lock = new Object();

    public void produce() throws InterruptedException {
        int goods = 0;

        while (true) {
            synchronized (lock) {
                if (store.size() > 10) {
                    System.out.println("producer is sleeping");
                    lock.wait();
                    System.out.println("producer awake");
                }
                store.add(goods);
                goods++;
                System.out.println("producer: " + goods);
                lock.notifyAll();
            }
        }
    }

    public void consume() throws InterruptedException {
        //int goods = store.getFirst();
        while (true) {
            synchronized (lock) {
                if (store.size() == 0) {
                    System.out.println("consumer is sleeping");
                    lock.wait();
                    System.out.println("consume awake");
                }
                int goodFromStore = store.remove();
                System.out.println("consume: " + goodFromStore);
                lock.notifyAll();
            }
        }
    }
}

//тоже самое только с массивом на 10 элементов