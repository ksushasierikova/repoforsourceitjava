package com.sourceit.bulls.and.cows;

import java.util.Arrays;
import java.util.Scanner;

public class MainBullsAndCows {

    public static void main(String[] args) {
        // int[] arrays = new int[4];
        // создать метод генерации цисла. Метод возващает массив int[] {3, 5, 8, 1}
        int[] guessedNum = generateArr();

        // главный цикл на 16 проходов
        for (int i = 0; i < 16; i++) {
            // вычитать строку из консоли
            // преобразовать в массив строк, а потом конвертировать в инт
            int[] userNumber = readUserNumber(i);

            System.out.println("your number is = ");
            printArray(userNumber);
            // TODO: 10.02.2018 логика игры
        }

    }

    private static void printArray(int[] userNumber) {
        for (int i = 0; i < userNumber.length; i++) {
            System.out.println("[" + userNumber[i] + "]");
        }
    }

    private static int[] readUserNumber(int i) {
        System.out.println("enter digit from 1 to 9");
        Scanner in = new Scanner(System.in);
        int[] arrS = new int[4];
        for (int j = 0; j < arrS.length; j++) {
            arrS[j] = in.nextInt();
        }

        return arrS;
    }

    private static int[] usersArray(){
        Scanner in = new Scanner(System.in);
        int[] arrS = new int[4];
        for (int i = 0; i < arrS.length; i++) {
            arrS[i] = in.nextInt();
        }
        return arrS;
    }

    private static int[] convertToIntArr(String myNumber) {
        String[] strToArray = myNumber.split("");
        int[] numArr = new int[strToArray.length];
        for (int i = 0; i < strToArray.length; i++) {
            numArr[i] = Integer.valueOf(strToArray[i]);
        }

        return numArr;
    }

    private static int[] generateArr() {
        int[] arrayF = getNewArr();

        return generateArr(arrayF);
    }

    // Recursion
    private static int[] generateArr(int[] arr) {
        if (uniq(arr)) {
            return arr;
        }
        int[] array = getNewArr();

        return generateArr(array);
    }

    public static boolean uniq(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    return false;
                }
            }
        }

        return true;
    }

    private static int[] getNewArr() {
        int[] array = new int[4];
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 8 + 1));
        }
        return array;
    }

    //        for (int i = 0; i < Arrays.toString(generateArr()).length(); i++){
//            for (int j = 0; j < arrS.length; j++)
//            {
//                if (i==j) continue;
//                if (arrS[i] == ){
//
//                }
//            }
//        }

}
