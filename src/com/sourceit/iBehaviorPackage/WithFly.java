package com.sourceit.iBehaviorPackage;

public class WithFly implements IBehavior {

    @Override
    public void fly() {
        System.out.println("Duck can't fly");
    }
}
