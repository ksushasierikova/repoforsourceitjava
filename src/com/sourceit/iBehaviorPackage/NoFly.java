package com.sourceit.iBehaviorPackage;

public class NoFly implements IBehavior {

    @Override
    public void fly() {
        System.out.println("Duck can fly");
    }
}
