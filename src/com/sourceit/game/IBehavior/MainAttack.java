package com.sourceit.game.IBehavior;

public class MainAttack {
    public static void main(String[] args) {
        Soldier soldierSpear = new Soldier(new SpearAttack());
        Soldier soldierBow = new Soldier(new BowAttack());
        Soldier soldierSword = new Soldier(new SwordAttack());

        soldierSpear.resolveAttack();
        soldierBow.resolveAttack();
        soldierSword.resolveAttack();

    }
}
