package com.sourceit.game.IBehavior;

public class Soldier {
    IBehavior attackBehavior;

    public Soldier(SpearAttack spearAttack) {
        this.attackBehavior = spearAttack;
    }

    public Soldier(BowAttack bowAttack) {
        this.attackBehavior = bowAttack;
    }

    public Soldier(SwordAttack swordAttack) {
        this.attackBehavior = swordAttack;
    }

    public void resolveAttack() {
        attackBehavior.attack();
    }
}
