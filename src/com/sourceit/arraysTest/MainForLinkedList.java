package com.sourceit.arraysTest;

/**
 * Created by P-C on 23.01.2018.
 */
public class MainForLinkedList {
    public static void main(String[] args) {
        LinkList<String> linkList = new LinkList<String>();
        linkList.add("1");
        linkList.add("2");
        linkList.add("3");
        linkList.add("4");
        linkList.add("5");
        linkList.add("6");
        linkList.remove("6");
        System.out.println(linkList.size());

//        LList<String> linkList = new LList<String>();
////        linkList.add("1");
////        linkList.add("2");
////        linkList.add("3");
////        linkList.add("4");
////        linkList.add("5");
////        linkList.add("6");
//        linkList.remove("6");
//        System.out.println(linkList.size());

    }

}
