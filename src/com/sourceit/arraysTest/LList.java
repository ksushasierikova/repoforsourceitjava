package com.sourceit.arraysTest;

public class LList<T> implements ICollection<T> {

    private Node first;
    private Node last;

    private int size = 0;

    @Override
    public void add(T value) {
        Node node = new Node(value);

        if (last == null) {
            first = node;
            last = node;
            size++;
        } else {
            last.next = node;
            last = node;
            size++;
        }
    }

    @Override
    public void remove(T value) {
        Node prev = null;
        Node curr = first;

        while (curr != null) {
            if (curr.t.equals(value)) {
                if (prev != null) {
                    prev.next = curr.next;
                    if (curr.next == null) {
                        last = prev;
                    }
                } else {
                    first = first.next;
                    if (first == null) {
                        last = null;
                    }
                }
                size--;
            }
            prev = curr;
            curr = curr.next;
        }
    }

    @Override
    public int size() {
        return size;
    }

    private class Node {

        public Node(T value) {
            t = value;
        }

        public T t;

        public Node next;

    }
}
