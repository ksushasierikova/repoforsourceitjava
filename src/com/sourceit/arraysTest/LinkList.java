package com.sourceit.arraysTest;

public class LinkList<T> implements ICollection<T> {
    Node first;
    Node last;

    private int size = 0;

    @Override
    public void add(T value) {
        Node node = new Node(value);

        if (last == null) {
            first = node;
            last = node;
            size++;
        } else {
            last.next = node;
            last = node;
            size++;
        }

    }

    @Override
    public void remove(T value) {

        if (last == null && first == null) {
            size = 0;
            System.out.println("No elements in list");
        } else if (first == last) {
            first = first.next;
            if (last == null) {
                first = null;
            }
            size = 0;
            System.out.println("single element was deleted");
        } else if (first.t.equals(value)) {
            first = first.next;
            System.out.println("first element was deleted");
            size--;
        } else if (last.t.equals(value)) {
            last = first.next;
            last.next = null;
            System.out.println("element from the end was deleted");
            size--;
        } else {
            first.next = last.next;
            System.out.println("element from the middle was deleted");
            size--;
        }

    }

    @Override
    public int size() {
        return size;
    }


    private class Node {
        T t;
        Node next;

        public Node(T value) {
            t = value;
        }

    }

}
