package com.sourceit.arraysTest;

public class ArrList<T> implements ICollection<T> {
    private T[] arr;
    private int idx = 0;



    public ArrList(int size) {
        arr = (T[]) new Object[size];
    }

    public ArrList() {
        arr = (T[]) new Object[6];
    }

    public void add(T value) {
        if (idx >= arr.length) {
            increase();
        }
        arr[idx++] = (T) value;
    }

    @Override
    public void remove(T indx) {

    }

    private void increase() {
        Object temp[] = new Object[arr.length * 2];
        for (int i = 0; i < arr.length; i++) {
            temp[i] = arr[i];
        }
        arr = (T[]) temp;
    }

    private void decrease() {
        Object temp[] = new Object[arr.length / 2];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = arr[i];
        }
        arr = (T[]) temp;
    }

    @Override
    public int size() {
        return idx;
    }

    public void remove(int indx) {
        for (int i = indx; i < idx; i++) {
            arr[i] = arr[i + 1];
        }
        arr[idx--] = null;

        if (idx <= arr.length / 2) {
            decrease();
        }
    }

}
