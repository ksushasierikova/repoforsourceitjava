package com.sourceit.arraysTest;

/**
 * Created by P-C on 23.01.2018.
 */
public interface ICollection<T> {

    public void add(T value);

    public void remove(T indx);

    public int size();

}
