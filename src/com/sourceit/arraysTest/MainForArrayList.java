package com.sourceit.arraysTest;

/**
 * Created by P-C on 23.01.2018.
 */
public class MainForArrayList {
    public static void main(String[] args) {

        ArrList<String> arrList = new ArrList<String>(4);
        arrList.add("one");
        arrList.add("two");
        arrList.add("3");
        arrList.add("4");
        arrList.add("5");

        arrList.remove(3);

        System.out.println("size after removal " + arrList.size());


    }
}
